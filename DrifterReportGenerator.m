%function DrifterReportGenerator(log_path)

%% Drifter Data from 06/03/2018 Mistral Test Flight at Table Mountain
% Psuedo-Lagrangian Drifter ground release. CU Boulder/ IRISS.

%% Plots
close all
clear all

import mlreportgen.report.*
import mlreportgen.dom.*


%%% Change these to match configuration of computer %%% 
addpath('/home/sswenson/Documents/MATLAB/zoharby-plot_google_map-f73f0ce')
fid = fopen('/home/sswenson/Desktop/0612/TRACK_1_RX_DATA_RAW.TXT','r');
fdata = textscan(fid, '%s','delimiter', '\n');
mesonet = wmsfind('mesonet*nexrad', 'SearchField', 'serverurl');
nexrad = mesonet.refine('nexrad-n0r', 'SearchField', 'any');


bid = [];
lat = {};
lon = {};
alt = {};
rng = {};
time = {};
j = [];

%% Import and Parse Data from .txt files
for i=1:length(fdata{1})
    data{i} = strsplit(fdata{1}{i});
    b = str2num(data{1,i}{1,3})+1;
    local = strsplit(data{i}{8},',');
    remote = strsplit(data{i}{9},',');
    if ismember(b,bid)
        
        lat{b} = [lat{b},str2num(remote{4})-0.000534];
        lon{b} = [lon{b},str2num(remote{5})+8.3885869];
        alt{b} = [alt{b},str2num(remote{6})];
        rng{b} = [rng{b},str2num(data{i}{5})];
        press{b} = [press{b},str2num(remote{7})];
        temp{b} = [temp{b}, str2num(remote{8})];
        % issue with data, make all humidity values positive
        hum{b} = [hum{b}, abs(str2num(remote{9}))];
        batt{b} = [batt{b},str2num(remote{11})];
        
        if isequal(local{2},'99')
            time{b}= [time{b},datenum(local{9},'HH:MM:SS.FFF')];
        else
            j = [j,i];
            time{b}= [time{b},datenum(local{8},'HH:MM:SS.FFF')];
        end
    
    else
        lat{b}=[];
        lat{b} = [lat{b},str2num(remote{4})-0.000534];
        lon{b}=[];
        lon{b} = [lon{b},str2num(remote{5})+8.3885869];
        alt{b}=[];
        alt{b} = [alt{b},str2num(remote{6})];
        rng{b}=[];
        rng{b} = [rng{b},str2num(data{i}{5})];
        press{b}=[];
        press{b} = [press{b},str2num(remote{7})];
        temp{b}=[];
        temp{b} = [temp{b}, str2num(remote{8})];
        hum{b}=[];
        hum{b} = [hum{b}, abs(str2num(remote{9}))];
        batt{b}=[];
        batt{b} = [batt{b},str2num(remote{11})];
        time{b}=[];
        if isequal(local{2},'99')
            time{b}= [time{b},datenum(local{9},'HH:MM:SS.FFF')];
        else
            j = [j,i];
            time{b}= [time{b},datenum(local{8},'HH:MM:SS.FFF')];
        end
        bid = [bid,b];
    end
end

 for i = 1:length(bid)
    %% Create Report
    b = bid(i)-1;
    k = j(1);
    td = strsplit(data{k}{8},',');
    td = td{7};
    td = strrep(td,'/','');
    tt = strsplit(data{k}{8},',');
    tt = tt{8}(1:5);
    tt = strrep(tt,':','');

    disp(b)
    rpt_fn = sprintf('%s_%sUTC_Drifter%d_Report',td,tt,b);
    rpt = Report(rpt_fn, 'pdf');
    
    %% Report
    tp = TitlePage;
    tp.Title = sprintf('06/11 Drifter %d Report',b);
    tp.Subtitle = 'CU Boulder Summer 2018 Deployment';
    tp.PubDate = '';
    add(rpt,tp)
    
    %text = fileread('DeploymentText.txt');
    %t = Paragraph(text);
    p1 = Paragraph('Drifter Hand Launch');
    add(rpt,p1)
    p2 = Paragraph('Takeoff Location: 36.6738637, -100.1229411');
    add(rpt,p2)
    p3 = Paragraph('Summary:');
    add(rpt,p3)
    p4 = Paragraph('Two receivers tracked the flight. Receiver 1 was hooked to the top of Tracker1, and was powered on for the duration of the flight. It lost connection to the drifter payload for ~30 minutes during flight, but received packets from the payload from 69.6 miles away ~1 hour after launch.');
    add(rpt,p4)
    p5 = Paragraph('Receiver 2 was hooked to the directional patch antenna. Approximately 5 min after launch, the cars had to relocate to a safer place relative to the storm. Receiver 2 and patch antenna were put into Tracker 2. The window in Tracker 2 was left open for as long as possible and the patch antenna was passed back and forth between passengers as the car turned, with the goal of always pointing it out the right window. The receiver came unplugged a few times in the car, and the signal was lost when we got caught in hail (~ same time Receiver1 lost connection). We switched from the patch antenna to a small omni antenna, but could not receive a signal from the payload. We eventually just powered it off.');
    add(rpt,p5)
    
    
    % Position Plot
    b = bid(i);
    fig(i)= figure;
    set(fig(i),'Visible','on','Units','inches','Position', [0, 0, 6, 3],'renderer','zbuffer')
    plot(lon{b}(1:1623),lat{b}(1:1623),'.r','MarkerSize',10)
    hold on
    plot(lon{b}(1624:1700),lat{b}(1624:1700),'.b','MarkerSize',10)
    plot_google_map('MapType', 'roadmao') 
    ylim([36.8, 36.9]);
    xlim([-100.4, -100.2]);
    title({'Position'},'interpreter','latex')
    add(rpt,Figure)
    
    % Altitude
    fig(i+1) = figure;
    set(fig(i+1),'Visible','off','Units', 'inches', 'Position', [0, 0, 8, 4])
    pointsize=10;
    scatter(lon{b},lat{b},pointsize,alt{b})
    plot_google_map
    c1{b,i} = colorbar;
    c1{b,i}.Label.String = 'Altitude (m MSL)';
    title({'Altitude vs. Position'},'interpreter','latex')
    add(rpt,Figure)
    
    fig(i+2) = figure;
    set(fig(i+2),'Visible','off','Units', 'Normalized', 'OuterPosition', [0, 0.5, 1, 0.6])
    plot(time{b},alt{b},'.','MarkerSize',6)
    datetick('x','HH:MM:SS')
    xtickangle(30)
    title({'Altitude vs. Time'},'interpreter','latex')
    add(rpt,Figure)

    %% Pressure Plots
    
    fig(i+3) = figure;
    set(fig(i+3),'Visible','off','Units', 'inches', 'Position', [0, 0, 8, 4])
    pointsize=10;
    scatter(lon{b},lat{b},pointsize,press{b})
    plot_google_map
    c2{b,i} = colorbar;
    c2{b,i}.Label.String = 'Pressure (hPa)';
    title({'Pressure vs. Position'},'interpreter','latex')
    add(rpt,Figure)
    
    fig(i+4) = figure;
    set(fig(i+4),'Visible','off','Units', 'Normalized', 'OuterPosition', [0, 0.5, 1, 0.55])
    plot(time{b},press{b},'.','MarkerSize',6)
    hold on
    datetick('x','HH:MM:SS')
    xtickangle(30)
    title({'Pressure vs. Time'},'interpreter','latex')
    add(rpt,Figure)
    
    %% Temperature Plots
    fig(i+5) = figure;
    set(fig(i+5),'Visible','off','Units', 'inches', 'Position', [0, 0, 8, 4])
    pointsize=10;
    scatter(lon{b},lat{b},pointsize,temp{b})
    plot_google_map
    c = colorbar;
    c.Label.String = 'Temperature (C)';
    title({'Temperature vs. Position'},'interpreter','latex')
    add(rpt,Figure)
    
    fig(i+6) = figure;
    set(fig(i+6),'Visible','off','Units', 'Normalized', 'OuterPosition', [0, 0.5, 1, 0.6])
    plot(time{b},temp{b},'.','MarkerSize',6)
    hold on
    datetick('x','HH:MM:SS')
    xtickangle(30)
    title({'Temperature vs. Time'},'interpreter','latex')
    add(rpt,Figure)
    
    %% Humidity Plots
    
    fig(i+7) = figure;
    set(fig(i+7),'Visible','off','Units', 'inches', 'Position', [0, 0, 8, 4])
    pointsize=10;
    scatter(lon{b},lat{b},pointsize,hum{b})
    plot_google_map
    c = colorbar;
    c.Label.String = 'Humidity (%)';
    c.TickLabelInterpreter = 'latex';
    title({'Humidity vs. Position'},'Interpreter','Latex')
    add(rpt,Figure)

    fig(i+8) = figure;
    set(fig(i+8),'Visible','off','Units', 'Normalized', 'OuterPosition', [0, 0.5, 1, 0.6])
    plot(time{b},hum{b},'.','MarkerSize',6)
    hold on
    datetick('x','HH:MM:SS')
    xtickangle(30)
    title({'Humidity vs. Time'},'interpreter','latex')
    add(rpt,Figure)
    
    close(rpt)

 end

disp('Done')
