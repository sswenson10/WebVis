import sys
sys.path.insert(0,'./protobuf')
import PyPacketLogger
from json import JSONDecoder
import re
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import csv

#log_name = raw_input("Enter log filename: ")
name = "20180529-110242_000"
name_pypl = name+'.pypl'

myPacketLogger = PyPacketLogger.PyPacketLogger()
myPacketLogger.openLogFile(name_pypl,True)

filename = name + '.json'
json_data = []

ID_array = []
drifter_lat = {}
drifter_lon = {}
drifter_alt = {}
drifter_press = {}
drifter_temp = {}
drifter_hum = {}
drifter_volt = {}
receiver_lat = {}
receiver_lon = {}
receiver_alt = {}
receiver_time = {}

NOT_WHITESPACE = re.compile(r'[^\s]')


# read logged data in as JSON
def decode_stacked(document, pos=0, decoder=JSONDecoder()):
    while True:
        match = NOT_WHITESPACE.search(document, pos)
        if not match:
            return
        pos = match.start()

        try:
            obj, pos = decoder.raw_decode(document, pos)
        except ValueError:
            # do something sensible if there's some error
            raise
        yield obj


def plot_colorline(x, y, c):
    c = cm.jet((c - np.min(c)) / (np.max(c) - np.min(c)))
    ax = plt.gca()
    for i in np.arange(len(x) - 1):
        ax.plot([x[i], x[i + 1]], [y[i], y[i + 1]], c=c[i])
    return


filen = open(filename, "r")
s = filen.read()

for obj in decode_stacked(s):
    json_data.append(obj)

# Parse data into dicts to plot more easily
for i in range(len(json_data)):
    ID = json_data[i]['balloon'][0]['ID']
    if ID not in ID_array:
        ID_array.append(ID)
        drifter_lat[ID] = []
        drifter_lon[ID] = []
        drifter_alt[ID] = []
        drifter_press[ID] = []
        drifter_temp[ID] = []
        drifter_hum[ID] = []
        drifter_volt[ID] = []
        receiver_lat[ID] = []
        receiver_lon[ID] = []
        receiver_alt[ID] = []
        receiver_time[ID] = []
        
    drifter_lat[ID].append(json_data[i]['balloon'][0]['LLAPos']['x'])
    drifter_lon[ID].append(json_data[i]['balloon'][0]['LLAPos']['y'])
    drifter_alt[ID].append(json_data[i]['balloon'][0]['LLAPos']['z'])
    drifter_press[ID].append(json_data[i]['balloon'][0]['pthsensor']['pressure'])
    drifter_temp[ID].append(json_data[i]['balloon'][0]['pthsensor']['temperature'])
    drifter_hum[ID].append(json_data[i]['balloon'][0]['pthsensor']['humidity'])
    drifter_volt[ID].append(json_data[i]['balloon'][0]['battery']['voltage'])
    receiver_lat[ID].append(json_data[i]['receiverLLAPos']['x'])
    receiver_lon[ID].append(json_data[i]['receiverLLAPos']['y'])
    receiver_alt[ID].append(json_data[i]['receiverLLAPos']['z'])
    receiver_time[ID].append(json_data[i]['time'])

for j in range(len(ID_array)):
    ID = ID_array[j]
    csvfilename = filename[0:-5]
    csvfilename = csvfilename + '_' + ID + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows([ID, drifter_lat[ID], drifter_lon[ID], drifter_alt[ID],
                          drifter_press[ID], drifter_temp[ID], drifter_hum[ID],
                          drifter_volt[ID], receiver_lat[ID], receiver_lon[ID],
                          receiver_alt[ID], receiver_time[ID]])




