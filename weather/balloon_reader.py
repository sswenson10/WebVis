'''
Balloon Reader is a process to read in from a board sensor that communicates with several (up to 4) drifting balloons for the summer research project
'''
import time
import sys
import Queue
import threading
import socket
import logging
import argparse
import serial


sys.path.insert(0,'../PyUAS')
sys.path.insert(0,'../PyUAS/protobuf')
import PyPacket
import PyPacketLogger
import PyPackets_pb2
import assorted_lib
#import PyPacketLogger doesn't exist until we push the code from desktop

shutdown_event = threading.Event()
msg_queue = Queue.Queue()

def write_buffer(drifter,remote_data,rem_id,rssi):

    if remote_data is not None:
        drifter.packetNum = int(remote_data[1])
        drifter.ID = str(rem_id)
        drifter.rssi = float(rssi)
        drifter.time = float(remote_data[9])
        drifter.LLA_Pos.x = float(remote_data[3])
        drifter.LLA_Pos.y = float(remote_data[4])
        drifter.LLA_Pos.z = float(remote_data[5])
        drifter.pthsensor.pressure = float(remote_data[6])
        drifter.pthsensor.temperature = float(remote_data[7])
        drifter.pthsensor.humidity = abs(float(remote_data[8]))
        drifter.battery.voltage = float(remote_data[10])
        drifter.Vel.x = float(remote_data[12])
        drifter.Vel.y = float(remote_data[11])
        drifter.Vel.z = float(remote_data[13][0:-1])
        

    else:
        drifter.packetNum = 1
        drifter.ID = str(0)
        drifter.rssi = 0
        drifter.time = 0
        drifter.LLA_Pos.x = 0
        drifter.LLA_Pos.y = 0
        drifter.LLA_Pos.z = 0
        drifter.pthsensor.pressure = 0
        drifter.pthsensor.temperature = 0
        drifter.pthsensor.humidity = 0
        drifter.battery.voltage = 0
        drifter.Vel.x = 0
        drifter.Vel.y = 0
        drifter.Vel.z = 0

     
def ParseData(fn,counter,myID,filename,rec):
    balloon_msg = PyPackets_pb2.Balloon_Sensor_Set_Msg()        
    balloon_msg.packetNum = counter
    balloon_msg.ID = myID
    balloon_msg.time = time.time()

    drifterStrs = []
    loop = True
    idVals = []
    i = 0
    if fn is not None:
        while loop:
            print idVals
            ap = fn.readline()
            filename = str(filename)
            save_file = open(filename[0:-5]+'.txt',"a")
            save_file.write(ap)
            save_file.close()
            time.sleep(.5)
            drifterStrs.append(ap)

            raw = drifterStrs[i].split()

            try:
                rem_id = raw[2]
            except IndexError:
                print IndexError

            if len(raw) < 3:
                return None

            elif len(idVals)>0:
                print("Repeat reached")
                loop = False

            elif '{' in raw[0]:
                print raw
                idVals.append(raw[2])
                drifter = balloon_msg.balloon.add()
                rssi = raw[3]
                raw_data = raw[6]

                local_data = raw[7].split(',')
                loc_vel = local_data[4]
                loc_course = local_data[5]
                loc_date = local_data[6]
                loc_time = local_data[7]

                remote_data = raw[8].split(',')

                balloon_msg.receiver = int(rec)
                balloon_msg.receiverLLA_Pos.x = float(local_data[1])
                balloon_msg.receiverLLA_Pos.y = float(local_data[2])
                balloon_msg.receiverLLA_Pos.z = float(local_data[3])
                balloon_msg.range = float(raw[4])
                balloon_msg.azimuth = float(raw[5])
                balloon_msg.course = float(local_data[5])
                balloon_msg.date = str(local_data[6])
                balloon_msg.rectime = str(local_data[7])
                balloon_msg.NumSV = str(local_data[8])

                write_buffer(drifter,remote_data,rem_id,rssi)
                i+=1

            else:
                return None


        print idVals
        balloon_msg.NumberOfBalloons = len(idVals)
    else:
        for k in range(2):
            drifter = balloon_msg.balloon.add()
            write_buffer(drifter, None, 0, 0)
            balloon_msg.receiverLLA_Pos.x = 0.0
            balloon_msg.receiverLLA_Pos.y = 1.0
            balloon_msg.receiverLLA_Pos.z = 2.0
        balloon_msg.NumberOfBalloons = 2

    return balloon_msg.SerializeToString()
        
class WritingThread(threading.Thread):
    def __init__(self, socket, NMPort,Logmode):
        threading.Thread.__init__(self)
        self.socket = socket
        self.NM_PORT = NMPort
		
        #Create logger
        self.logger = logging.getLogger("BalloonReader:WritingThread")
        self.logger.setLevel(Logmode)
        myhandler = logging.StreamHandler()
        self.logger.addHandler(myhandler)
        self.logger.info("Writing Thread has started")
  
    def run(self):
	
        #Don't care about subscribers at this point
        while not shutdown_event.is_set():
            try:
                next_msg = msg_queue.get_nowait()
            except Queue.Empty:
                time.sleep(0.01)
            else:
                self.socket.sendto(next_msg,('localhost',self.NM_PORT))
                #Log message  
                self.logger.info("Sent Message to Network Manager")
    				
        #End of while loop
        self.socket.close()
        #Log message
        self.logger.info("Closing Writing Thread")
		
class ReadFromSensor(threading.Thread):
	def __init__(self,serialPort,readrate,myIDnum,Logmode,recNum):
         threading.Thread.__init__(self)
        #Create loggers and put them here     
         self.logger = logging.getLogger("BalloonReader:SensorThread")
         self.logger.setLevel(Logmode)
         myhandler = logging.StreamHandler()
         self.logger.addHandler(myhandler)
         self.logger.info("Sensor Thread has started")
         self.rec = recNum
         
         #self.serialPortname = serialPort
         if serialPort is None:
             self.fn = None
         else:
            self.fn = serial.Serial(port = serialPort)
            #self.fn = open(serialPort)
         #check that this opened and throw a critical error otherwise
         self.readrate = readrate
         
         thisid = PyPacket.PacketID(PyPacket.PacketPlatform.AIRCRAFT,myIDnum)
         self.MYID = str(thisid.getBytes())
         fn = time.strftime("%Y%m%d-%H%M%S")
         self.packet_log = PyPacketLogger.PyPacketLogger(fn)
         self.packet_log.initFile()
         self.logger.info("Logging Sensor Packets to: %s", self.packet_log.logname)
         
         self.PyPkt = PyPacket.PyPacket()
         self.PyPkt.setDataType(PyPacket.PacketDataType.PKT_BALLOON_SENSOR_SET)
         self.PyPkt.setID(thisid.getBytes())

	
	def run(self):
         counter = 0
         
         while not shutdown_event.is_set():
             counter += 1
             datastr = ParseData(self.fn,counter,self.MYID,self.packet_log.logname,self.rec)
             if datastr is not None:
                 #print(len(datastr))
                 #self.PyPkt.displayPacket()
                 self.PyPkt.setData(datastr)
                 self.packet_log.writePacketToLog(self.PyPkt)
                 msg_queue.put(self.PyPkt.getPacket())
                 self.logger.info("Packet Built and added to Queue")
             else:
                 time.sleep(0.1)
             #log message for pyPacketlogger when added
        #endof while loop
        #send log message ending loop
		
		
'''
main runtime
'''
if __name__ == "__main__":
	#create logger
	
	#Arguments?
    parser = argparse.ArgumentParser(description='Balloon Sensor Reader for Drifters')
    parser.add_argument("COMMPORT",type=str)
    parser.add_argument("BALLOON",type=int)
    parser.add_argument("RECEIVER", type=int)
	
    args = parser.parse_args()
	
    COMMPORT = args.COMMPORT
    if COMMPORT == "None":
        COMMPORT = None
    
    numid = args.BALLOON
    NM_PORT = 16000 #hardcoded for now
    SensorRate = 0.25 #1 second

    recNum = args.RECEIVER
    #Create socket
    s_out = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	
    #start each of the threads
    Logmode = logging.DEBUG
    Sensor = ReadFromSensor(COMMPORT,SensorRate,numid,Logmode,recNum)
    Sensor.start()
    wthread = WritingThread(s_out,NM_PORT,Logmode)
    wthread.start()

    while threading.active_count() > 3:
        try:
            time.sleep(1)
        except (KeyboardInterrupt, SystemExit):
            shutdown_event.set()
            #log messages
			
    sys.exit()