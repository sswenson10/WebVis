# Drifter Web Visualization #

## Install tornado and protobuf

### Linux/Windows/OSx
From the command line run *pip install tornado*, *pip install protobuf* 

## Install protobuf compiler

### Linux 
1. From the terminal run *sudo apt-get install protobuf-compiler*

### Windows

1. Download visual studio 
    - choose: dekstop development w/ C++ (choose all options for windows)

2. From the terminal, *cd* to the file that contains *bootstrap-vcpkg.bat* and enter *run bootstrap-vcpkg.bat*.
3. Run *vcpkg install protobuf[zlib] protobuf[zlib]:x64-windows*

### OSx
1. Install brew
2. *brew install protobuf*
    - if errors occur try running *brew doctor* and follow any recommended fixes. If this doesn't work, try *brew upgrade protobuf*
If using brew doesn't work, try:
1. Download the appropriate release from:  https://github.com/google/protobuf/releases
2. Unzip the folder and run *./autogen.sh && ./configure && make* 
	- if you run into error: **autoreconf: failed to run aclocal: No such file or directory* run *brew install autoconf && brew install automake*
3. Go through step 2 again. 
4. Run:
    *make check*
    *sudo make install*
    *which protoc*
    *protoc --version*

## Run Program 
Once all the packages are installed, download all the filees from https://gitlab.com/sswenson10/WebVis (weather, PyUAS, oasis). Find USB port the receiver is connected to (SERIAL PORT). 
Open three terminal windows.
1. cd to WebVis/weather
2. cd to WebVis/PyUAS 
3. cd to WebVis/oasis

### In the weather terminal

Run *python balloon_reader.py SERIAL PORT 90 1*
* SERIAL PORT: serial port connected to receiver, along the lines of */dev/ttyUSB0*, etc. **If you are getting an error when running this program, the serial port is most likely the culprit**. Check to make sure you are specifying the correct serial port (use TeraTerm in windows, *screen* osX, *cat* in Linux).  
* if running in LINUX, will often get a permission denied error. If this happens first try:
    *sudo usermod -a -G dialout $USER*
    *sudo chmod a+rw SERIAL PORT*
    log out and log in again for changes to take affect.
    If that doesn't work, run *sudo chmod 666 SERIAL PORT* everytime the receiver is plugged in. 
* make sure end of line reads '90 1'
* this program reads from the receiver and parses the dat into JSON format for the Network manager
* output should look something like:
    		**['2']
			115 
			Packet Built and added to Queue
			Sent Message to Network Manager**

### In the PyUAS terminal
Run *python NetworkManagerProcess.py*
* this program sends the data to the network manager
* output should look something like:
			**Starting!
			1527614030.61
			received "49" from ('127.0.0.1', 52736)
			Received Node Heartbeat**

### In the oasis terminal
Run *python server_app_weather.py*
* this program manages the server and starts the website for visualization
* output should look something like:
			**Created Web Server Side
			Created WebReaderSide
			StartedPublishing Thread
			Running with 2 Readers
			Started Server Thread**


## Viewing Data
After starting the programs, open an internet browser and enter: *localhost:8888*. The server is set in server_app_weather.py so if the page doesn't load, make sure the localhost hasn't been changed. Another common port is *localhost:8080*. If you have checked and the webpage still isn't loading, something is likely wrong with the server app or the balloon reader, check server_app_weather.py and balloon_reader.py terminal line outputs. 

Press the **start** button in the upper right corner of the window. It it is working, the balloon icon should show up on the map. 

NOTE: The balloon number for the tabs doesn't correspond to the balloon ID but to the order the receiver detects them. The ID number is displayed along with the rest of the information in the balloon tab. 

To save, navigate to the control tab and press **Start Recording** 
NOTE: The file WILL NOT save to the file name in the text box. Instead it will correspond to the timestamp of when the program was started. 
 
## Logged Files
The data is saved three times throughout the process of collecting the data: on a SD card on the receiver, when balloon_reader.py is initiated, and when **Start/Stop Recording** is pressed on the controls tab.


### Receiver Data
The receiver logs all the data that it receives to a SD card. The data loaded last should be the last log file on the card. This data is the most reliable however it is all of the data from the time the receiver begins seeing data. 

### balloon_reader.py
The python program begins logging data when it is called. The .pypl files are stored in the weather folder in the format YEARMONTHDAY-BalloonLog_000.pypl where the last number corresponds to the iteration of the log for that day. Everytime balloon_reader.py is called, a new log is created. In the PyUAS folder, there is a program PyUAS_Log_Reader that will reformat the data as jsons. Json_CVS_converter.py will conver the json data to cvs to be more easily read by MATLAB.


### Start/Stop Recording on Web Page
The Stat/Stop Recording button under the Record tab, creates a new file everytime a new instance of server_app_weather.py is created. After that, the start/stop function records to that file. The output is also .pypl so the same functions as listed above can be used to output json and csv files. 

## Stop Program
There are still a few bugs in the system for stopping the code. From the webpage, to stop updating the data at anytime just click the **stop** button in the upper right hand corner. To kill specific programs use Ctrl-C from the command line in Windows and Ctr-Shift-\ in Linux.

## Convert Data type
The data that is logged by the receiver will be in .txt format. The data map can be found here: https://docs.google.com/spreadsheets/d/1_f2m2-iIW1vuYAy8mT2Ox5BI9vdtZutXYUpK1ughBQY/edit#gid=0.

For the data logged by the program, the output will be a binary file with the extension .pypl. This data needs to be converted in order to read it it into MATLAB, etc. In the folder PyUAS the program *PyUAS_Log_Reader.py* will output the data in both .json and .csv formats. The data file needs to be moved to the PyUAS folder and in the python program, the variable **name** needs to be changed to correspond to the data file. The .csv can then be read into MATLAB and plotted. 

## Generating a Simple Report of the Data
In the WebVis folder, there is a MATLAB function *DrifterReporterGenerator.m*. In order to create a report for each drifter, fill out the *DeploymentText.txt* with relavant information about the test/deployment. Then if running Linux, from the terminal run *matlab -r 'try DrifterReportGenerator(PATH TO LOG FILE); catch; end; quit*. This should produce a simple .pdf that contains the information you entered in *DeploymentText.txt* as well as the information logged by the receiver, plotted. 
